package com.sf.sgs.mark.exception.domain;

import java.io.Serializable;

/**
 * Created by 01204808 on 2017/7/16.
 */
public class KafkaData implements Serializable {

    /**  */
	private static final long serialVersionUID = -3919944256092419890L;

	private String id;

    private String waybillNo;

    private String topicName;

    private String message;

    private String createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

}
