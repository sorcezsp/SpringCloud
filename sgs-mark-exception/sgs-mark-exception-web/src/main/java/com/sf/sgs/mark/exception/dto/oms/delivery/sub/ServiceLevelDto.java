package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

public class ServiceLevelDto {

	private String provider;

	private ProductDto product;

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public ProductDto getProduct() {
		return product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}
}
