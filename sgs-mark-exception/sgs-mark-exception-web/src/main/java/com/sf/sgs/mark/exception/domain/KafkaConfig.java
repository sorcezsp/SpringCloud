package com.sf.sgs.mark.exception.domain;


/**
 * Created by 01204808 on 2017/7/16.
 */
public class KafkaConfig {

    /**
     * 主题名称
     */
    private String topicName;

    /**
     * 主题校验码
     */
    private String topicToken;

    /**
     * kafka地址
     */
    private String kafkaUrl;

    /**
     * 集群
     */
    private String clusterName;

    /**
     * 线程数量
     */
    private Integer threadCount;

    /**
     * 主题描述
     */
    private String topicDescription;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicToken() {
        return topicToken;
    }

    public void setTopicToken(String topicToken) {
        this.topicToken = topicToken;
    }

    public String getKafkaUrl() {
        return kafkaUrl;
    }

    public void setKafkaUrl(String kafkaUrl) {
        this.kafkaUrl = kafkaUrl;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }

    public String getTopicDescription() {
        return topicDescription;
    }

    public void setTopicDescription(String topicDescription) {
        this.topicDescription = topicDescription;
    }
}
