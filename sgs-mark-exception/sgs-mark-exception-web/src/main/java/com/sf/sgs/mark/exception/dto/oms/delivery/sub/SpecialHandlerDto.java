package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 特殊操作详情
 * 
 * @author 01139951
 *
 */
public class SpecialHandlerDto {

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 操作码
	 */
	private String code;
	/**
	 * 操作信息，以；分割
	 */
	private String operation;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
}
