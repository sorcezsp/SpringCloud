package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

import java.util.List;


/**
 * 增值服务
 * 
 * @author 01139951
 *
 */
public class AddServiceDto {

	/**
	 * 增值服务编码
	 */
	private String code;
	private String attr1;
	private String attr2;
	private String attr3;
	private String attr4;
	private String attr5;

	private List<FeeDto> feeList;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAttr1() {
		return attr1;
	}

	public void setAttr1(String attr1) {
		this.attr1 = attr1;
	}

	public String getAttr2() {
		return attr2;
	}

	public void setAttr2(String attr2) {
		this.attr2 = attr2;
	}

	public String getAttr3() {
		return attr3;
	}

	public void setAttr3(String attr3) {
		this.attr3 = attr3;
	}

	public String getAttr4() {
		return attr4;
	}

	public void setAttr4(String attr4) {
		this.attr4 = attr4;
	}

	public String getAttr5() {
		return attr5;
	}

	public void setAttr5(String attr5) {
		this.attr5 = attr5;
	}

	public List<FeeDto> getFeeList() {
		return feeList;
	}

	public void setFeeList(List<FeeDto> feeList) {
		this.feeList = feeList;
	}
}
