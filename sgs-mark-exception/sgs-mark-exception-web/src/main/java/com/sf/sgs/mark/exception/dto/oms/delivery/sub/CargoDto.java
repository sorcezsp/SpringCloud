package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 拖寄物
 * 
 * @author 854272
 * @date2016年10月18日上午10:41:08
 */
public class CargoDto {

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 单位
	 */
	private String uom;

	/**
	 * 数量
	 */
	private Double quantity;

	/**
	 * 价格
	 */
	private Double price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}