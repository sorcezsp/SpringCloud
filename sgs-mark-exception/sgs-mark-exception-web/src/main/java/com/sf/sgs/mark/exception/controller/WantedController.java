package com.sf.sgs.mark.exception.controller;

import com.sf.sgs.mark.exception.common.constant.KafkaConstants;
import com.sf.sgs.mark.exception.common.utils.DateUtils;
import com.sf.sgs.mark.exception.common.utils.KafkaUtils;
import com.sf.sgs.mark.exception.domain.KafkaData;
import com.sf.sgs.mark.exception.service.IKafkaDataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/wanted")
public class WantedController {
	
	@Autowired
	private IKafkaDataService kafkaDataService;
	
    @RequestMapping(value = "/sendWantedData", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> sendWantedData(String waybillNo, int wantedType, String info) {
        //{id:60557319,code:null,waybillNO:"444836538071",dataType:3,info:"C类快件",dealTm:"2017-07-01 17:58:28",dealCount:0,dealFlg:1,dealIp:"127084@cnsz22pl0105",insertTm:"2017-07-01 17:57:43"}
        int id = (int) (89999999 * Math.random()) + 10000000;
        StringBuffer sb = new StringBuffer("{");
        sb.append("id:").append(id).append(",code:null,").append("waybillNO:\"").append(waybillNo).append("\",dataType:").append(wantedType)
                .append(",info:\"").append(info).append("\",dealTm:\"").append(DateUtils.dateToStr(new Date()))
                .append("\",dealCount:0,dealFlg:1,dealIp:\"127084@cnsz22pl0105\",insertTm:\"").append(DateUtils.dateToStr(new Date())).append("\"}");
        String message = sb.toString();
        KafkaData kafkaData = new KafkaData();
        kafkaData.setId(UUID.randomUUID().toString());
        kafkaData.setTopicName(KafkaConstants.WAYBILL_EXCEPTION_KAFKA_TOPIC);
        kafkaData.setMessage(message);
        kafkaData.setWaybillNo(waybillNo);
        kafkaData.setCreateTime(DateUtils.dateToStr(new Date()));
        kafkaDataService.addKafkaData(kafkaData);
        Map<String, Object> map = new HashMap<>();
        map.put("success", true);
        return map;
    }

    public static void main(String[] args) {
        int id = (int) (89999999 * Math.random()) + 10000000;
        StringBuffer sb = new StringBuffer("{");
        sb.append("id:").append(id).append(",code:null,").append("waybillNO:\"").append("093598825604").append("\",dataType:").append(3)
                .append(",info:\"").append("C类快件").append("\",dealTm:\"").append(DateUtils.dateToStr(new Date()))
                .append("\",dealCount:0,dealFlg:1,dealIp:\"127084@cnsz22pl0105\",insertTm:\"").append(DateUtils.dateToStr(new Date())).append("\"}");
        String message = sb.toString();
        System.out.print(message);
        KafkaUtils.sendMessage(KafkaConstants.KAFKA_URL, KafkaConstants.WAYBILL_EXCEPTION_KAFKA_TOPIC, KafkaConstants.WAYBILL_EXCEPTION_KAFKA_TOPIC_TOKEN, KafkaConstants.KAFKA_CLUSTER_NAME, message);
        
    }
    
    
}
