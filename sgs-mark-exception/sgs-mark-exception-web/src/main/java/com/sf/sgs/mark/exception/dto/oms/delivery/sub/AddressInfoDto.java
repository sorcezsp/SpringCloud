package com.sf.sgs.mark.exception.dto.oms.delivery.sub;


/**
 * 地址信息
 * @author 01139951
 *
 */
public class AddressInfoDto {
private String country;
    
    private String province;
    
    private String city;
    /**
     * 
     */
    private String county;
    /**
     * 街道
     */
    private String street;
    /**
     * 具体地址
     */
    private String address;
    /**
     * 邮编
     */
    private String postalCode;
    /**
     * 格式有三种：城市代码、 城市代码-网点代码、城市代码-网点代码-单元区域代码
     * 城市-网点-单元区域（755-755A-755A00101）
     */
    private String locationCode;
    
    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
    
}
