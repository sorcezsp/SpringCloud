package com.sf.sgs.mark.exception.dto.oms.delivery;

import java.util.Date;
import java.util.List;

import com.sf.sgs.mark.exception.dto.oms.delivery.sub.ActionDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.AddServiceDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.AttrDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.CargoDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.DestinationInfoDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.ExtendPropertyDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.FeeDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.OriginateInfoDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.ServiceLevelDto;
import com.sf.sgs.mark.exception.dto.oms.delivery.sub.WaybillDto;

/**
 * oms派件消息实体
 * @author 01139951
 *
 */
public class DeliveryOmsRequestDto {
    
	/**
	 * 预约号，分库字段
	 */
	private String txid;

	/**
	 * 时间戳
	 */
	private Long timestamp;

	/**
	 * 运单信息
	 */
	private WaybillDto waybill;

	private String checkWord;

	/**
	 * 预约派件开始时间
	 */
	private Date expectStartTm;

	/**
	 * 预约派件结束时间
	 */
	private Date expectFinishTm;

	/**
	 * 动态预计派送时间
	 */
	private String dynExpcDeliveryTm;

	/**
	 * 备注
	 */
	private String waybillRemark;

	/**
	 * 签回单号
	 */
	private String signedBackWaybillNo;

	/**
	 * 寄件人
	 */
	private OriginateInfoDto originateInfo;

	/**
	 * 收件人
	 */
	private DestinationInfoDto destinationInfo;

	/**
	 * 重量
	 */
	private Double metaWeight;

	/**
	 * 实际重量
	 */
	private Double realWeight;

	/**
	 * 服务等级
	 */
	private ServiceLevelDto levelOfService;

	/**
	 * 运输费用
	 */
	private FeeDto freightFee;
	
	/**
	 * 收件员
	 */
	private String consigneeEmpCode;

	/**
	 * 增值服务列表
	 */
	private List<AddServiceDto> serviceList;

	/**
	 * 托寄物列表
	 */
	private List<CargoDto> cargoList;

	/**
	 * 扩展属性
	 */
	private List<ExtendPropertyDto> userDefList;

    private List<AttrDto> attrs;// 扩展字段
	
	/**
	 * 派件变更信息,更改标识
	 */
	private ActionDto action;

	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public WaybillDto getWaybill() {
		return waybill;
	}

	public void setWaybill(WaybillDto waybill) {
		this.waybill = waybill;
	}

	public String getCheckWord() {
		return checkWord;
	}

	public void setCheckWord(String checkWord) {
		this.checkWord = checkWord;
	}

	public Date getExpectStartTm() {
		return expectStartTm;
	}

	public void setExpectStartTm(Date expectStartTm) {
		this.expectStartTm = expectStartTm;
	}

	public Date getExpectFinishTm() {
		return expectFinishTm;
	}

	public void setExpectFinishTm(Date expectFinishTm) {
		this.expectFinishTm = expectFinishTm;
	}

	public OriginateInfoDto getOriginateInfo() {
		return originateInfo;
	}

	public void setOriginateInfo(OriginateInfoDto originateInfo) {
		this.originateInfo = originateInfo;
	}

	public DestinationInfoDto getDestinationInfo() {
		return destinationInfo;
	}

	public void setDestinationInfo(DestinationInfoDto destinationInfo) {
		this.destinationInfo = destinationInfo;
	}

	public Double getMetaWeight() {
		return metaWeight;
	}

	public void setMetaWeight(Double metaWeight) {
		this.metaWeight = metaWeight;
	}

	public Double getRealWeight() {
		return realWeight;
	}

	public void setRealWeight(Double realWeight) {
		this.realWeight = realWeight;
	}

	public ServiceLevelDto getLevelOfService() {
		return levelOfService;
	}

	public void setLevelOfService(ServiceLevelDto levelOfService) {
		this.levelOfService = levelOfService;
	}

	public FeeDto getFreightFee() {
		return freightFee;
	}

	public void setFreightFee(FeeDto freightFee) {
		this.freightFee = freightFee;
	}

	public List<AddServiceDto> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<AddServiceDto> serviceList) {
		this.serviceList = serviceList;
	}

	public List<CargoDto> getCargoList() {
		return cargoList;
	}

	public void setCargoList(List<CargoDto> cargoList) {
		this.cargoList = cargoList;
	}

	public List<ExtendPropertyDto> getUserDefList() {
		return userDefList;
	}

	public void setUserDefList(List<ExtendPropertyDto> userDefList) {
		this.userDefList = userDefList;
	}

	public ActionDto getAction() {
		return action;
	}

	public void setAction(ActionDto action) {
		this.action = action;
	}

	public String getDynExpcDeliveryTm() {
		return dynExpcDeliveryTm;
	}

	public void setDynExpcDeliveryTm(String dynExpcDeliveryTm) {
		this.dynExpcDeliveryTm = dynExpcDeliveryTm;
	}

	public String getWaybillRemark() {
		return waybillRemark;
	}

	public void setWaybillRemark(String waybillRemark) {
		this.waybillRemark = waybillRemark;
	}

	public String getSignedBackWaybillNo() {
		return signedBackWaybillNo;
	}

	public void setSignedBackWaybillNo(String signedBackWaybillNo) {
		this.signedBackWaybillNo = signedBackWaybillNo;
	}

	public List<AttrDto> getAttrs() {
		return attrs;
	}

	public void setAttrs(List<AttrDto> attrs) {
		this.attrs = attrs;
	}

	public String getConsigneeEmpCode() {
		return consigneeEmpCode;
	}

	public void setConsigneeEmpCode(String consigneeEmpCode) {
		this.consigneeEmpCode = consigneeEmpCode;
	}
    
}
