package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * <pre>
 * 变更内容
 * SSS地址补录信息触发改派件任务,或者CX到方信息变更\变更联系人姓名和电话时:  desitionInfoChange为 true
 * SSS网点信息或单元区域信息时: locationCodeChange为true
 * OMPS预计派送时间:  dynExpcDeliveryTmChange为true
 * OMS接收运单信息触发改派件任务时：infoUpdate为true，如果目的地与原来的派件任务目的地城市不一样时：desitionInfoChange=true, locationCodeChange为true
 * WEM红冲运单修改触发（只有清单时才触发）改派件任务时：infoUpdate为true，如果目的地相关信息有修改，则desitionInfoChange=true，如果目的地城市有修改locationCodeChange为true
 * </pre>
 * 
 * @author 854272
 * @date2016年10月18日上午10:36:18
 */
public class ActionDto {

	/**
	 * 若为update时，全量修改
	 */
	private boolean infoUpdate;

	/**
	 * SSS地址补录信息触发改派件任务,或者CX到方信息变更\变更联系人姓名和电话
	 */
	private boolean desitionInfoChange;

	/**
	 * 派件时间变更
	 */
	private boolean timeChange;

	/**
	 * SSS网点信息或单元区域信息
	 */
	private boolean locationCodeChange;

	/**
	 * OMPS预计派送时间
	 */
	private boolean dynExpcDeliveryTmChange;

	/**
	 * 派件方式变更--改自提，具体信息在SpecialHandle
	 */
	private String specialHandlers;

	public boolean isInfoUpdate() {
		return infoUpdate;
	}

	public void setInfoUpdate(boolean infoUpdate) {
		this.infoUpdate = infoUpdate;
	}

	public boolean isDesitionInfoChange() {
		return desitionInfoChange;
	}

	public void setDesitionInfoChange(boolean desitionInfoChange) {
		this.desitionInfoChange = desitionInfoChange;
	}

	public boolean isTimeChange() {
		return timeChange;
	}

	public void setTimeChange(boolean timeChange) {
		this.timeChange = timeChange;
	}

	public String getSpecialHandlers() {
		return specialHandlers;
	}

	public void setSpecialHandlers(String specialHandlers) {
		this.specialHandlers = specialHandlers;
	}

	public boolean isLocationCodeChange() {
		return locationCodeChange;
	}

	public void setLocationCodeChange(boolean locationCodeChange) {
		this.locationCodeChange = locationCodeChange;
	}

	public boolean isDynExpcDeliveryTmChange() {
		return dynExpcDeliveryTmChange;
	}

	public void setDynExpcDeliveryTmChange(boolean dynExpcDeliveryTmChange) {
		this.dynExpcDeliveryTmChange = dynExpcDeliveryTmChange;
	}

	/**
	 * <handler> <type>32</type> <code>32009</code>
	 * <operation>0:09001200,12001800,18002200;1:09002200</operation> </handler>
	 * 说明：0：工作日；1：周末及节假日； 多个时间段用逗号分隔；如果0之后的时间段为空，代表不派送；
	 * 如果为09002200代表全天派送；09002200：代表09:00-22:00 <handler> <type>32</type>
	 * <code>32010</code> <operation>0</operation> </handler>
	 * 说明：0:未设置；1：电话勿扰；2：短信勿扰 ；默认为0
	 * 
	 */
}