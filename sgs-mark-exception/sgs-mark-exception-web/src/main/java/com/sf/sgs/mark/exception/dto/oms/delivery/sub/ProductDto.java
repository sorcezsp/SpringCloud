package com.sf.sgs.mark.exception.dto.oms.delivery.sub;


public class ProductDto {
	
	/**
	 * 编码,填充格式为：时效类型,快件内容,业务类型（limit_type_code,cargo_type_code,express_type_code才半角逗号分隔）
	 */
    private String code;
    
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
