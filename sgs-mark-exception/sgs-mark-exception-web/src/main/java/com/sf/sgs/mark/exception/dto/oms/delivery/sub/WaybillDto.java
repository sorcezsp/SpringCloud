package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 运单详情
 * 
 * @author 854272
 * @date2016年10月18日上午10:59:24
 */
public class WaybillDto {

	private String mainNo;

	private ChildListDto childList;

	public String getMainNo() {
		return mainNo;
	}

	public void setMainNo(String mainNo) {
		this.mainNo = mainNo;
	}

	public ChildListDto getChildList() {
		return childList;
	}

	public void setChildList(ChildListDto childList) {
		this.childList = childList;
	}

}
