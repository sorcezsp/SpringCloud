package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

import java.util.List;

/**
 * 到方信息
 * 
 * @author 01139951
 *
 */
public class DestinationInfoDto {

	private String contact; // 名字

	private String phone; // 手机

	private String mobile; // 电话

	private String company; // 公司

	private AddressInfoDto addressInfo;// 地址

	/**
	 * 特殊操作
	 */
	private List<SpecialHandlerDto> handlerList;

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public List<SpecialHandlerDto> getHandlerList() {
		return handlerList;
	}

	public void setHandlerList(List<SpecialHandlerDto> handlerList) {
		this.handlerList = handlerList;
	}

	public AddressInfoDto getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(AddressInfoDto addressInfo) {
		this.addressInfo = addressInfo;
	}
}
