/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.sf.sgs.mark.exception.dto;

import java.io.Serializable;

/**
 * 描述：
 * 
 * <pre>
 * HISTORY
 * ****************************************************************************
 *  ID   DATE           PERSON          REASON
 *  1    2017年7月17日      01204808         Create
 * ****************************************************************************
 * </pre>
 * 
 * @author 01204808
 * @since 1.0
 */
public class DeliveryChangeDto implements Serializable {

	/**  */
	private static final long serialVersionUID = -1386824050023920161L;

	private String waybillNo;

	private int deliveryChangeType;

	private String deliveryDate;

	private String deliveryStartTime;

	private String deliveryEndTime;

	private String deptName;

	private String deptCode;

	private String deptAddress;

	private String userName;

	private String mobile;

	private String workdayPreference;

	private String holidayPreference;

	public String getWaybillNo() {
		return waybillNo;
	}

	public void setWaybillNo(String waybillNo) {
		this.waybillNo = waybillNo;
	}

	public int getDeliveryChangeType() {
		return deliveryChangeType;
	}

	public void setDeliveryChangeType(int deliveryChangeType) {
		this.deliveryChangeType = deliveryChangeType;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryStartTime() {
		return deliveryStartTime;
	}

	public void setDeliveryStartTime(String deliveryStartTime) {
		this.deliveryStartTime = deliveryStartTime;
	}

	public String getDeliveryEndTime() {
		return deliveryEndTime;
	}

	public void setDeliveryEndTime(String deliveryEndTime) {
		this.deliveryEndTime = deliveryEndTime;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptAddress() {
		return deptAddress;
	}

	public void setDeptAddress(String deptAddress) {
		this.deptAddress = deptAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getWorkdayPreference() {
		return workdayPreference;
	}

	public void setWorkdayPreference(String workdayPreference) {
		this.workdayPreference = workdayPreference;
	}

	public String getHolidayPreference() {
		return holidayPreference;
	}

	public void setHolidayPreference(String holidayPreference) {
		this.holidayPreference = holidayPreference;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
