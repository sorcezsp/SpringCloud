package com.sf.sgs.mark.exception.dto.oms.delivery;

import com.sf.sgs.mark.exception.dto.oms.delivery.sub.WaybillDto;

/**
 * CX派件变更消息实体
 * 
 * @author 854272
 * @date2016年10月18日上午10:35:16
 */
public class DeliveryChangeRequestDto {

	private WaybillDto waybill;

	public WaybillDto getWaybill() {
		return waybill;
	}

	public void setWaybill(WaybillDto waybill) {
		this.waybill = waybill;
	}

}
