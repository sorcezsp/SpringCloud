package com.sf.sgs.mark.exception.controller;

import com.sf.sgs.mark.exception.common.utils.DateUtils;
import com.sf.sgs.mark.exception.domain.KafkaData;
import com.sf.sgs.mark.exception.service.IKafkaDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by 01204808 on 2017/7/16.
 */
@Controller
@RequestMapping("/kafkaData")
public class KafkaDataContoller {

	@Autowired
	private IKafkaDataService kafkaDataService;

	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryAll(String waybillNo, String topicName, int page, int rows) {
		Map<String, Object> map = new HashMap<>();
		List<KafkaData> dataList = kafkaDataService.queryKafkaData(waybillNo, topicName, (page - 1) * rows, rows);
		map.put("total", kafkaDataService.queryKafkaDataTotal(waybillNo, topicName));
		map.put("rows", dataList);
		return map;
	}

	@RequestMapping(value = "/sendKafkaData")
	@ResponseBody
	public Map<String, Object> sendKafkaData(KafkaData kafkaData) {
		Map<String, Object> map = new HashMap<>();
		kafkaData.setId(UUID.randomUUID().toString());
		kafkaData.setCreateTime(DateUtils.dateToStr(new Date()));
		boolean num = kafkaDataService.addKafkaData(kafkaData);
		if (num) {
			map.put("success", true);
		} else {
			map.put("success", false);
			map.put("errorMessage", "数据发生失败！");
		}
		return map;
	}
}
