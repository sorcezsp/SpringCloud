package com.sf.sgs.mark.exception.service.impl;

import com.sf.sgs.mark.exception.domain.KafkaConfig;
import com.sf.sgs.mark.exception.mapper.KafkaConfigMapper;
import com.sf.sgs.mark.exception.service.IKafkaConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by 01204808 on 2017/7/16.
 */
@Component
public class KafkaConfigService implements IKafkaConfigService {

    @Autowired
    private KafkaConfigMapper kafkaConfigMapper;


    @Override
    public List<KafkaConfig> getKafkaConfigAll() {
        return kafkaConfigMapper.getKafkaConfigAll();
    }

    @Override
    public List<KafkaConfig> queryKafkaConfig(String topicName, int startRecord, int pageSize) {
        return kafkaConfigMapper.getKafkaConfigByPage(topicName, startRecord, pageSize);
    }

    @Override
    public KafkaConfig getKafkaConfig(String topicName) {
        return kafkaConfigMapper.getKafkaConfig(topicName);
    }

    @Override
    public int getKafkaConfigTotal() {
        return kafkaConfigMapper.getKafkaConfigTotal();
    }

    @Override
    public boolean addKafkaConfig(KafkaConfig kafkaConfig) {
        return kafkaConfigMapper.addKafkaConfig(kafkaConfig) > 0;
    }

    @Override
    public boolean deleteKafkaConfig(String topicName) {
        return kafkaConfigMapper.deleteKafkaConfig(topicName) > 0;
    }

    @Override
    public boolean editKafkaConfig(KafkaConfig kafkaConfig) {
        return kafkaConfigMapper.editKafkaConfig(kafkaConfig) > 0;
    }
}
