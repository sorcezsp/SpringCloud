package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 费用信息
 * 
 * @author 01139951
 *
 */
public class FeeDto {

	/**
	 * 费用编码
	 */
	private String code;

	/**
	 * 金额
	 */
	private Double amt;

	/**
	 * 付款方式，1、寄付 2、到付 3、第三方付
	 */
	private String payMethod;
	/**
	 * 月结卡号
	 */
	private String custid;

	/**
	 * 收付款网点
	 */
	private String gatherZoneCode;

	/**
	 * 结算方式
	 */
	private String settlementTypeCode;

	/**
	 * 付款方式变更
	 */
	private String paymentChangeTypeCode;

	/**
	 * 币别代码
	 */
	private String currencyCode;

	/**
	 * 交款人
	 */
	private String gatherEmpCode;

	/**
	 * 业务所属地区编码
	 */
	private String bizOwnerZoneCode;

	/**
	 * 原寄地货款
	 */
	private Double sourceCodeFeeAmt;

	/**
	 * 汇率（目的地到原寄地）
	 */
	private Double exchangeRate;

	/**
	 * 目的地所在币种
	 */
	private String destCurrencyCode;

	/**
	 * 个性化费用
	 */
	private Double feeAmtInd;

	/**
	 * 个性化类型
	 */
	private String feeIndType;

	/**
	 * 计价卡号
	 */
	private String valutionAcctCode;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getAmt() {
		return amt;
	}

	public void setAmt(Double amt) {
		this.amt = amt;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getGatherZoneCode() {
		return gatherZoneCode;
	}

	public void setGatherZoneCode(String gatherZoneCode) {
		this.gatherZoneCode = gatherZoneCode;
	}

	public String getSettlementTypeCode() {
		return settlementTypeCode;
	}

	public void setSettlementTypeCode(String settlementTypeCode) {
		this.settlementTypeCode = settlementTypeCode;
	}

	public String getPaymentChangeTypeCode() {
		return paymentChangeTypeCode;
	}

	public void setPaymentChangeTypeCode(String paymentChangeTypeCode) {
		this.paymentChangeTypeCode = paymentChangeTypeCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getGatherEmpCode() {
		return gatherEmpCode;
	}

	public void setGatherEmpCode(String gatherEmpCode) {
		this.gatherEmpCode = gatherEmpCode;
	}

	public String getBizOwnerZoneCode() {
		return bizOwnerZoneCode;
	}

	public void setBizOwnerZoneCode(String bizOwnerZoneCode) {
		this.bizOwnerZoneCode = bizOwnerZoneCode;
	}

	public Double getSourceCodeFeeAmt() {
		return sourceCodeFeeAmt;
	}

	public void setSourceCodeFeeAmt(Double sourceCodeFeeAmt) {
		this.sourceCodeFeeAmt = sourceCodeFeeAmt;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getDestCurrencyCode() {
		return destCurrencyCode;
	}

	public void setDestCurrencyCode(String destCurrencyCode) {
		this.destCurrencyCode = destCurrencyCode;
	}

	public Double getFeeAmtInd() {
		return feeAmtInd;
	}

	public void setFeeAmtInd(Double feeAmtInd) {
		this.feeAmtInd = feeAmtInd;
	}

	public String getFeeIndType() {
		return feeIndType;
	}

	public void setFeeIndType(String feeIndType) {
		this.feeIndType = feeIndType;
	}

	public String getValutionAcctCode() {
		return valutionAcctCode;
	}

	public void setValutionAcctCode(String valutionAcctCode) {
		this.valutionAcctCode = valutionAcctCode;
	}
}