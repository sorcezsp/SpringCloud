package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

import java.util.List;


public class ChildListDto {

	private List<String> childList;

	public List<String> getChildList() {
		return childList;
	}

	public void setChildList(List<String> childList) {
		this.childList = childList;
	}

}
