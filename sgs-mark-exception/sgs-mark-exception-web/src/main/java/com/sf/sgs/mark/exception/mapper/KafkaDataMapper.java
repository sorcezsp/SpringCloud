package com.sf.sgs.mark.exception.mapper;

import com.sf.sgs.mark.exception.domain.KafkaData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 01204808 on 2017/7/16.
 */
public interface KafkaDataMapper {

    int addKafkaData(KafkaData kafkaData);

    List<KafkaData> findKafkaData(@Param("waybillNo") String waybillNo, @Param("topicName") String topicName, @Param("startRecord") int startRecord, @Param("pageSize") int pageSize);

    Integer getKafkaDataTotal(@Param("waybillNo") String waybillNo, @Param("topicName") String topicName);
}
