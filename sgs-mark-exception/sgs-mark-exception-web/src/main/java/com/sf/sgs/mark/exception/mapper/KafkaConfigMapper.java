package com.sf.sgs.mark.exception.mapper;

import com.sf.sgs.mark.exception.domain.KafkaConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 01204808 on 2017/7/16.
 */
public interface KafkaConfigMapper {

    List<KafkaConfig> getKafkaConfigAll();

    List<KafkaConfig> getKafkaConfigByPage(@Param("topicName") String topicName, @Param("startRecord") int startRecord, @Param("pageSize") int pageSize);

    KafkaConfig getKafkaConfig(@Param("topicName") String topicName);

    int getKafkaConfigTotal();

    int addKafkaConfig(KafkaConfig kafkaConfig);

    int deleteKafkaConfig(@Param("topicName") String topicName);

    int editKafkaConfig(KafkaConfig kafkaConfig);
}
