package com.sf.sgs.mark.exception.service;

import com.sf.sgs.mark.exception.domain.KafkaConfig;

import java.util.List;

/**
 * Created by 01204808 on 2017/7/16.
 */
public interface IKafkaConfigService {

    /**
     * 获取全部主题配置
     * @return
     */
    List<KafkaConfig> getKafkaConfigAll();

    /**
     * 分页获取主题配置
     * @param topicName
     * @param startRecord
     * @param pageSize
     * @return
     */
    List<KafkaConfig> queryKafkaConfig(String topicName, int startRecord, int pageSize);

    /**
     * 根据主题名称获取主题
     * @param topicName
     * @return
     */
    KafkaConfig getKafkaConfig(String topicName);

    /**
     * 获取kafka配置数量
     * @return
     */
    int getKafkaConfigTotal();

    /**
     * 添加kafka配置
     * @param kafkaConfig
     * @return
     */
    boolean addKafkaConfig(KafkaConfig kafkaConfig);

    /**
     * 删除kafka配置
     * @param topicName
     * @return
     */
    boolean deleteKafkaConfig(String topicName);

    /**
     * 修改kafka配置
     * @param kafkaConfig
     * @return
     */
    boolean editKafkaConfig(KafkaConfig kafkaConfig);
}
