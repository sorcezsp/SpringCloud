package com.sf.sgs.mark.exception.common.constant;

/**
 * Created by 01204808 on 2017/7/14.
 */
public class KafkaConstants {
    public static final String KAFKA_URL = "http://mommon_st.intsit.sfdc.com.cn:1080/mom-mon/monitor/requestService.pub";
    public static final String WAYBILL_EXCEPTION_KAFKA_TOPIC = "SSS_RMS_TO_EXP";
    public static final String DELIVERY_CHANGE_OMS_KAFKA_TOPIC = "SHIVA_OMS_SGS_DELIVERY_ABNORMAL";
    public static final String WAYBILL_EXCEPTION_KAFKA_TOPIC_TOKEN = "92*$Gg3I";
    public static final String DELIVERY_CHANGE_OMS_KAFKA_TOPIC_TOKEN = "12345678";
    public static final String KAFKA_CLUSTER_NAME = "sfst";
}
