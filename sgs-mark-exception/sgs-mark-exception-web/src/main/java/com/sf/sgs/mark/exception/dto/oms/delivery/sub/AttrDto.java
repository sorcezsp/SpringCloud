package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 
 * 描述：扩展字段DTO
 * 
 * <pre>
 * HISTORY
 * ****************************************************************************
 *  ID   DATE           PERSON          REASON
 *  1    2017年3月20日        866321         Create
 * ****************************************************************************
 * </pre>
 * 
 * @author 866321
 */
public class AttrDto {

	private String attr001;// 扩展字段1

	private String attr002;// 扩展字段2

	private String attr003;// 扩展字段3

	private String attr004;// 扩展字段4

	private String attr005;// 扩展字段5

	private String attr006;// 扩展字段6

	private String attr007;// 扩展字段7

	private String attr008;// 扩展字段8

	private String attr009;// 扩展字段9

	private String attr010;// 扩展字段10

	private String attr011;// 扩展字段11

	private String attr012;// 扩展字段12

	private String attr013;// 扩展字段13

	private String attr014;// 扩展字段14

	private String attr015;// 扩展字段15

	private String attr016;// 扩展字段16

	private String attr017;// 扩展字段17

	private String attr018;// 扩展字段18

	private String attr019;// 扩展字段19

	private String attr020;// 扩展字段20

	private String attr021;// 扩展字段21

	private String attr022;// 扩展字段22

	private String attr023;// 扩展字段23

	private String attr024;// 扩展字段24

	private String attr025;// 扩展字段25

	private String attr026;// 扩展字段26

	private String attr027;// 扩展字段27

	private String attr028;// 扩展字段28

	private String attr029;// 扩展字段29

	private String attr030;// 扩展字段30

	private String attr031;// 扩展字段31

	private String attr032;// 扩展字段32

	private String attr033;// 扩展字段33

	private String attr034;// 扩展字段34

	private String attr035;// 扩展字段35

	private String attr036;// 扩展字段36

	private String attr037;// 扩展字段37

	private String attr038;// 扩展字段38

	private String attr039;// 扩展字段39

	private String attr040;// 扩展字段40

	private String attr041;// 扩展字段41

	private String attr042;// 扩展字段42

	private String attr043;// 扩展字段43

	private String attr044;// 扩展字段44

	private String attr045;// 扩展字段45

	private String attr046;// 扩展字段46

	private String attr047;// 扩展字段47

	private String attr048;// 扩展字段48

	private String attr049;// 扩展字段49

	private String attr050;// 扩展字段50

	public String getAttr001() {
		return attr001;
	}

	public void setAttr001(String attr001) {
		this.attr001 = attr001;
	}

	public String getAttr002() {
		return attr002;
	}

	public void setAttr002(String attr002) {
		this.attr002 = attr002;
	}

	public String getAttr003() {
		return attr003;
	}

	public void setAttr003(String attr003) {
		this.attr003 = attr003;
	}

	public String getAttr004() {
		return attr004;
	}

	public void setAttr004(String attr004) {
		this.attr004 = attr004;
	}

	public String getAttr005() {
		return attr005;
	}

	public void setAttr005(String attr005) {
		this.attr005 = attr005;
	}

	public String getAttr006() {
		return attr006;
	}

	public void setAttr006(String attr006) {
		this.attr006 = attr006;
	}

	public String getAttr007() {
		return attr007;
	}

	public void setAttr007(String attr007) {
		this.attr007 = attr007;
	}

	public String getAttr008() {
		return attr008;
	}

	public void setAttr008(String attr008) {
		this.attr008 = attr008;
	}

	public String getAttr009() {
		return attr009;
	}

	public void setAttr009(String attr009) {
		this.attr009 = attr009;
	}

	public String getAttr010() {
		return attr010;
	}

	public void setAttr010(String attr010) {
		this.attr010 = attr010;
	}

	public String getAttr011() {
		return attr011;
	}

	public void setAttr011(String attr011) {
		this.attr011 = attr011;
	}

	public String getAttr012() {
		return attr012;
	}

	public void setAttr012(String attr012) {
		this.attr012 = attr012;
	}

	public String getAttr013() {
		return attr013;
	}

	public void setAttr013(String attr013) {
		this.attr013 = attr013;
	}

	public String getAttr014() {
		return attr014;
	}

	public void setAttr014(String attr014) {
		this.attr014 = attr014;
	}

	public String getAttr015() {
		return attr015;
	}

	public void setAttr015(String attr015) {
		this.attr015 = attr015;
	}

	public String getAttr016() {
		return attr016;
	}

	public void setAttr016(String attr016) {
		this.attr016 = attr016;
	}

	public String getAttr017() {
		return attr017;
	}

	public void setAttr017(String attr017) {
		this.attr017 = attr017;
	}

	public String getAttr018() {
		return attr018;
	}

	public void setAttr018(String attr018) {
		this.attr018 = attr018;
	}

	public String getAttr019() {
		return attr019;
	}

	public void setAttr019(String attr019) {
		this.attr019 = attr019;
	}

	public String getAttr020() {
		return attr020;
	}

	public void setAttr020(String attr020) {
		this.attr020 = attr020;
	}

	public String getAttr021() {
		return attr021;
	}

	public void setAttr021(String attr021) {
		this.attr021 = attr021;
	}

	public String getAttr022() {
		return attr022;
	}

	public void setAttr022(String attr022) {
		this.attr022 = attr022;
	}

	public String getAttr023() {
		return attr023;
	}

	public void setAttr023(String attr023) {
		this.attr023 = attr023;
	}

	public String getAttr024() {
		return attr024;
	}

	public void setAttr024(String attr024) {
		this.attr024 = attr024;
	}

	public String getAttr025() {
		return attr025;
	}

	public void setAttr025(String attr025) {
		this.attr025 = attr025;
	}

	public String getAttr026() {
		return attr026;
	}

	public void setAttr026(String attr026) {
		this.attr026 = attr026;
	}

	public String getAttr027() {
		return attr027;
	}

	public void setAttr027(String attr027) {
		this.attr027 = attr027;
	}

	public String getAttr028() {
		return attr028;
	}

	public void setAttr028(String attr028) {
		this.attr028 = attr028;
	}

	public String getAttr029() {
		return attr029;
	}

	public void setAttr029(String attr029) {
		this.attr029 = attr029;
	}

	public String getAttr030() {
		return attr030;
	}

	public void setAttr030(String attr030) {
		this.attr030 = attr030;
	}

	public String getAttr031() {
		return attr031;
	}

	public void setAttr031(String attr031) {
		this.attr031 = attr031;
	}

	public String getAttr032() {
		return attr032;
	}

	public void setAttr032(String attr032) {
		this.attr032 = attr032;
	}

	public String getAttr033() {
		return attr033;
	}

	public void setAttr033(String attr033) {
		this.attr033 = attr033;
	}

	public String getAttr034() {
		return attr034;
	}

	public void setAttr034(String attr034) {
		this.attr034 = attr034;
	}

	public String getAttr035() {
		return attr035;
	}

	public void setAttr035(String attr035) {
		this.attr035 = attr035;
	}

	public String getAttr036() {
		return attr036;
	}

	public void setAttr036(String attr036) {
		this.attr036 = attr036;
	}

	public String getAttr037() {
		return attr037;
	}

	public void setAttr037(String attr037) {
		this.attr037 = attr037;
	}

	public String getAttr038() {
		return attr038;
	}

	public void setAttr038(String attr038) {
		this.attr038 = attr038;
	}

	public String getAttr039() {
		return attr039;
	}

	public void setAttr039(String attr039) {
		this.attr039 = attr039;
	}

	public String getAttr040() {
		return attr040;
	}

	public void setAttr040(String attr040) {
		this.attr040 = attr040;
	}

	public String getAttr041() {
		return attr041;
	}

	public void setAttr041(String attr041) {
		this.attr041 = attr041;
	}

	public String getAttr042() {
		return attr042;
	}

	public void setAttr042(String attr042) {
		this.attr042 = attr042;
	}

	public String getAttr043() {
		return attr043;
	}

	public void setAttr043(String attr043) {
		this.attr043 = attr043;
	}

	public String getAttr044() {
		return attr044;
	}

	public void setAttr044(String attr044) {
		this.attr044 = attr044;
	}

	public String getAttr045() {
		return attr045;
	}

	public void setAttr045(String attr045) {
		this.attr045 = attr045;
	}

	public String getAttr046() {
		return attr046;
	}

	public void setAttr046(String attr046) {
		this.attr046 = attr046;
	}

	public String getAttr047() {
		return attr047;
	}

	public void setAttr047(String attr047) {
		this.attr047 = attr047;
	}

	public String getAttr048() {
		return attr048;
	}

	public void setAttr048(String attr048) {
		this.attr048 = attr048;
	}

	public String getAttr049() {
		return attr049;
	}

	public void setAttr049(String attr049) {
		this.attr049 = attr049;
	}

	public String getAttr050() {
		return attr050;
	}

	public void setAttr050(String attr050) {
		this.attr050 = attr050;
	}

}
