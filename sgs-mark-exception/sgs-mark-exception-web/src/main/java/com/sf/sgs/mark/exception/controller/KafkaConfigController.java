package com.sf.sgs.mark.exception.controller;

import com.sf.sgs.mark.exception.domain.KafkaConfig;
import com.sf.sgs.mark.exception.service.IKafkaConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 01204808 on 2017/7/16.
 */
@Controller
@RequestMapping("/kafkaConfig")
public class KafkaConfigController {

    @Autowired
    private IKafkaConfigService kafkaConfigService;

    @RequestMapping(value = "/queryTopic")
    @ResponseBody
    public List<KafkaConfig> getKafkaConfigTopic() {
        return kafkaConfigService.getKafkaConfigAll();
    }

    @RequestMapping(value = "/query")
    @ResponseBody
    public Map<String, Object> getKafkaConfigAll(String topicName, int page, int rows) {
        Map<String, Object> map = new HashMap<>();
        List<KafkaConfig> configList = kafkaConfigService.queryKafkaConfig(topicName, (page - 1) * rows, rows);
        map.put("rows", configList);
        map.put("total", kafkaConfigService.getKafkaConfigTotal());
        return map;
    }

    @RequestMapping(value = "/add")
    @ResponseBody
    public Map<String, Object> addKafkaConfig(KafkaConfig kafkaConfig) {
        Map<String, Object> map = new HashMap<>();
        boolean num = kafkaConfigService.addKafkaConfig(kafkaConfig);
        if (num) {
            map.put("success", true);
        } else {
            map.put("success", false);
            map.put("errorMessage", "数据发送失败！");
        }
        return map;
    }

    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> deleteKafkaConfig(String topicName) {
        Map<String, Object> map = new HashMap<>();
        boolean num = kafkaConfigService.deleteKafkaConfig(topicName);
        if (num) {
            map.put("success", true);
        } else {
            map.put("success", false);
            map.put("errorMessage", "数据发送失败！");
        }
        return map;
    }

    @RequestMapping(value = "/edit")
    @ResponseBody
    public Map<String, Object> editKafkaConfig(KafkaConfig kafkaConfig) {
        Map<String, Object> map = new HashMap<>();
        boolean num = kafkaConfigService.editKafkaConfig(kafkaConfig);
        if (num) {
            map.put("success", true);
        } else {
            map.put("success", false);
            map.put("errorMessage", "数据发送失败！");
        }
        return map;
    }
}
