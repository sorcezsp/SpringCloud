package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 扩展信息
 * 
 * @author 854272
 * @date2016年10月18日上午11:01:03
 */
public class ExtendPropertyDto {

	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
