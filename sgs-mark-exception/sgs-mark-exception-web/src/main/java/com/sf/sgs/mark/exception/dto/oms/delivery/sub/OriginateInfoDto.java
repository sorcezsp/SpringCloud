package com.sf.sgs.mark.exception.dto.oms.delivery.sub;

/**
 * 寄方信息
 * 
 * @author 01139951
 *
 */
public class OriginateInfoDto {
	private String contact; // 名字

	private String phone; // 手机

	private String mobile; // 电话

	private String company; // 公司

	private AddressInfoDto addressInfo;// 地址

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public AddressInfoDto getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(AddressInfoDto addressInfo) {
		this.addressInfo = addressInfo;
	}
}
