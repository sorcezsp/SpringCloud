package com.sf.sgs.mark.exception.service;

import com.sf.sgs.mark.exception.domain.KafkaConfig;
import com.sf.sgs.mark.exception.domain.KafkaData;

import java.util.List;

/**
 * Created by 01204808 on 2017/7/16.
 */
public interface IKafkaDataService {

	boolean addKafkaData(KafkaData kafkaData);

	List<KafkaData> queryKafkaData(String waybillNo, String topicName, int startRecord, int pageSize);

	int queryKafkaDataTotal(String waybillNo, String topicName);
}
