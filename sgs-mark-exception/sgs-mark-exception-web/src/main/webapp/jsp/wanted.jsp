<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<head>
<base href="<%=basePath%>">

<title>KAFKA数据发送工具</title>
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/gray/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/demo/demo.css">
<script type="text/javascript" src="jquery-easyui-1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/jsonformat.js"></script>
<style type="text/css">
body {
	margin: 0px;
	padding: 0px;
}

#fm {
	margin: 0;
	padding: 10px 20px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}
</style>
<script type="text/javascript">
	$(function() {
		$('#dg').datagrid({
			url : 'me/kafkaData/query',
			loadMsg : '加载中，请稍后···',
			pagination : 'true',
			rownumbers : 'true',
			fit : 'true',
			fitColumns : 'true',
			singleSelect : 'true',
			striped : 'true',
			toolbar : '#opr-button',
			queryParams:{'topicName':$("#topic-name").val()}
		});

		var pager = $('#dg').datagrid('getPager');
		$(pager)
				.pagination(
						{
							displayMsg : '总共有<span style="color:red;">{total}</span>条记录，当前显示第<span style="color:red;">{from}</span>条到<span style="color:red;">{to}</span>条记录',
							beforePageText : '第',
							afterPageText : '页 共 {pages} 页',
						});

		$.extend($.fn.validatebox.defaults.rules, {
			waybillNo : {
				validator : function(value) {
					var reg = /^[0-9]{12}$/;
					return reg.test(value);
				},
				message : '请输入12位数字'
			}
		});

	});
</script>
<script type="text/javascript">
	var url;
	function add() {
		$('#dlg').dialog('open').dialog('setTitle', '通缉业务数据发送');
		$('#fm').form('clear');
		url = 'me/wanted/sendWantedData';
	}

	function save() {
		$('#fm').form('submit', {
			url : url,
			onSubmit : function() {
				return $(this).form('validate');
			},
			success : function(result) {
				var data = jQuery.parseJSON(result);
				if (data.success) {
					$.messager.alert({
						title : '消息',
						msg : '数据发送成功！',
						icon:'info'
					});
					$('#dlg').dialog('close');
					$('#dg').datagrid('reload');
					$('#fm').form('clear');
				} else {
					$.messager.show({
						title : '错误',
						msg : data.errorMessage
					});
				}
			}
		});
	}

	function refresh() {
		$('#dg').datagrid('reload');
	}

	function doQuery() {
		/* var queryParams = $('#dg').datagrid('options').queryParams;  
		alert($("#topic-query").combobox('getValue'));
		queryParams.topicName = $("#topic-query").combobox('getValue');  
		$('#dg').datagrid('options').queryParams=queryParams;        
		$("#dg").datagrid('reload');  */
		$('#dg').datagrid('load', {
			"topicName" : 'SSS_RMS_TO_EXP',
			"waybillNo" : $("#waybillNo-query").textbox('getValue')
		});
	}
</script>
</head>
<body>
	<div id="opr-button" class="toolbar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td style="width: 10px">&nbsp;</td>
				<td style="font-size: 12px;">运单号：</td>
				<td><input id="waybillNo-query" class="easyui-textbox" /></td>
				<td style="width: 10px">&nbsp;</td>
				<td><a class="easyui-linkbutton" iconCls="icon-search"
					plain="true" onclick="doQuery()">查询</a></td>
				<td><a class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="add()">发送</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="refresh()">刷新</a></td>
			</tr>
		</table>
	</div>
	<table id="dg">
		<thead>
			<tr>
				<th field="waybillNo" width="80">运单号</th>
				<th field="topicName" width="80">主题名称</th>
				<th field="message" width="80">数据内容</th>
				<th field="createTime" width="120">发送时间</th>

			</tr>
		</thead>
	</table>
	<div id="dlg" class="easyui-dialog"
		data-options="closed:'true',buttons:'#dlg-button',modal:true"
		style="padding: 10px 10px">
		<!--<div class="ftitle">通缉业务数据发送</div>-->
		<form id="fm" method="post" novalidate="novalidate">
			<table>
				<tr>
					<td width="80px"><label style="font-size: 12px;">通缉类型：</label></td>
					<td><input id="wanted-type" name="wantedType"
						class="easyui-combobox" style="width: 200px"
						data-options="valueField: 'id',textField: 'text',url:'json/wantedtype.json',required:true,missingMessage:'请选择通缉类型',editable:false" /></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">运单号：</label></td>
					<td><input id="waybill-no" name="waybillNo"
						class="easyui-textbox"
						data-options="missingMessage:'请输入运单号',required:'true',validType:'waybillNo'"
						style="width: 200px"></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">通缉内容：</label></td>
					<td><input id="wanted-info" name="info" class="easyui-textbox"
						data-options="multiline:true,required:'true',missingMessage:'请输入通缉内容'"
						style="height: 200px; width: 300px"></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="dlg-button">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-ok'" onclick="save()">保存</a> <a
			href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-cancel'"
			onclick="javascript:$('#dlg').dialog('close')">取消</a>
	</div>
	
	<input id="topic-name" type="hidden" value="SSS_RMS_TO_EXP">
</body>
</html>

