<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<head>
<base href="<%=basePath%>">

<title>KAFKA数据发送工具</title>
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/gray/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/demo/demo.css">
<script type="text/javascript" src="jquery-easyui-1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/jsonformat.js"></script>
<style type="text/css">
body {
	margin: 0px;
	padding: 0px;
}

#fm {
	margin: 0;
	padding: 10px 20px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}
</style>
<script type="text/javascript">
	$(function() {
		$('#dg').datagrid({
			url : 'me/kafkaConfig/query',
			loadMsg : '加载中，请稍后···',
			pagination : 'true',
			rownumbers : 'true',
			fit : 'true',
			fitColumns : 'true',
			singleSelect : 'true',
			striped : 'true',
			toolbar : '#opr-button',
			queryParams:{'topicName':$("#topic-name").val()}
		});

		var pager = $('#dg').datagrid('getPager');
		$(pager)
				.pagination(
						{
							displayMsg : '总共有<span style="color:red;">{total}</span>条记录，当前显示第<span style="color:red;">{from}</span>条到<span style="color:red;">{to}</span>条记录',
							beforePageText : '第',
							afterPageText : '页 共 {pages} 页',
						});

		$.extend($.fn.validatebox.defaults.rules, {
			waybillNo : {
				validator : function(value) {
					var reg = /^[0-9]{12}$/;
					return reg.test(value);
				},
				message : '请输入12位数字'
			}
		});

	});
</script>
<script type="text/javascript">
	var url;
	function add() {
		$('#dlg').dialog('open').dialog('setTitle', 'kafka配置');
		$('#fm').form('clear');
		url = 'me/kafkaConfig/add';
	}

	function save() {
		$('#fm').form('submit', {
			url : url,
			onSubmit : function() {
				return $(this).form('validate');
			},
			success : function(result) {
				var data = jQuery.parseJSON(result);
				if (data.success) {
					$.messager.alert({
						title : '消息',
						msg : '数据发送成功！',
						icon:'info'
					});
					$('#dlg').dialog('close');
					$('#dg').datagrid('reload');
					$('#fm').form('clear');
				} else {
					$.messager.show({
						title : '错误',
						msg : data.errorMessage
					});
				}
			}
		});
	}

	function refresh() {
		$('#dg').datagrid('reload');
	}

	function doQuery() {
		$('#dg').datagrid('load', {
			"topicName" : $("#topic-query").textbox('getValue')
		});
	}

    function remove() {
        var row = $('#dg').datagrid('getSelected');
        if (row) {
            $.messager.confirm('提示', '您确认删除该条记录吗？', function(r) {
                if (r) {
                    $.post('me/kafkaConfig/delete', {
                        topicName : row.topicName
                    }, function(result) {
                        if (result.success) {
                            $('#dg').datagrid('reload');
                        } else {
                            $.messager.show({
                                title : 'Error',
                                msg : result.errorMsg
                            });
                        }
                    }, 'json');
                }
            });

        }
    }

    function edit() {
        $('#topic-name').validatebox({
            required : false
        });
        var row = $('#dg').datagrid('getSelected');
        if (row) {
            $('#dlg').dialog('open').dialog('setTitle', '编辑');
            $('#fm').form('load', row);
            $('#topic-name').attr('disabled', 'disabled');
            url = 'me/kafkaConfig/edit';
        }
    }
</script>
</head>
<body>
	<div id="opr-button" class="toolbar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td style="width: 10px">&nbsp;</td>
				<td style="font-size: 12px;">主题名称：</td>
				<td><input id="topic-query" class="easyui-textbox" /></td>
				<td style="width: 10px">&nbsp;</td>
				<td><a class="easyui-linkbutton" iconCls="icon-search"
					plain="true" onclick="doQuery()">查询</a></td>
				<td><a class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="add()">增加</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a class="easyui-linkbutton" iconCls="icon-edit"
					   plain="true" onclick="edit()">编辑</a></td>
				<div class="datagrid-btn-separator"></div></td>
				<td><a class="easyui-linkbutton" iconCls="icon-remove"
					   plain="true" onclick="remove()">删除</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a class="easyui-linkbutton" iconCls="icon-reload"
					plain="true" onclick="refresh()">刷新</a></td>
			</tr>
		</table>
	</div>
	<table id="dg">
		<thead>
			<tr>
				<th field="topicName" width="120">主题名称</th>
				<th field="topicDescription" width="120">主题描述</th>
				<th field="topicToken" width="40">主题校验码</th>
				<th field="kafkaUrl" width="120">kafka地址</th>
				<th field="clusterName" width="30">集群名称</th>
				<th field="threadCount" width="20">线程数量</th>
			</tr>
		</thead>
	</table>
	<div id="dlg" class="easyui-dialog"
		data-options="closed:'true',buttons:'#dlg-button',modal:true"
		style="padding: 10px 10px">
		<!--<div class="ftitle">kafka配置</div>-->
		<form id="fm" method="post" novalidate="novalidate">
			<table>
				<tr>
					<td width="80px"><label style="font-size: 12px;">主题名称：</label></td>
					<td><input id="topic-name" name="topicName"
						class="easyui-textbox" style="width: 200px"
						data-options="valueField: 'id',textField: 'text',required:true,missingMessage:'请输入主题名称'" /></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">主题描述：</label></td>
					<td><input id="topic-description" name="topicDescription"
						class="easyui-textbox"
						data-options="multiline:true,missingMessage:'请输入主题描述',required:'true'"
						style="width: 200px;height: 50px"></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">校验码：</label></td>
					<td><input id="topic-token" name="topicToken" class="easyui-textbox"
						data-options="required:'true',missingMessage:'请输入主题校验码'"
							   style="width: 200px"></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">kafka地址：</label></td>
					<td><input id="kafka-url" name="kafkaUrl"
							   class="easyui-textbox" style="width: 200px;height: 50px;"
							   data-options="multiline:true,required:true,missingMessage:'请输入kafka地址'" /></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">集群：</label></td>
					<td><input id="cluster-name" name="clusterName"
							   class="easyui-textbox"
							   data-options="missingMessage:'请输入集群',required:'true'"
							   style="width: 200px"></td>
				</tr>
				<tr>
					<td width="80px"><label style="font-size: 12px;">线程数量：</label></td>
					<td><input id="thread-count" name="threadCount" class="easyui-numberbox"
							   data-options="required:'true',missingMessage:'请输入线程数量'"
							   style="width: 200px"></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="dlg-button">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-ok'" onclick="save()">保存</a> <a
			href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-cancel'"
			onclick="javascript:$('#dlg').dialog('close')">取消</a>
	</div>
	
</body>
</html>

