<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ "/" + request.getContextPath() + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>KAFKA数据发送工具</title>
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/gray/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/demo/demo.css">
<script type="text/javascript" src="jquery-easyui-1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
<script type="text/javascript">
	function addTab(title, url) {
		if ($('#tabs').tabs('exists', title)) {
			$('#tabs').tabs('select', title);
		} else {
			var content = '<iframe scrolling="no" frameborder="0"  src="' + url
					+ '" style="width:100%;height:100%;"></iframe>';
			$('#tabs').tabs('add', {
				title : title,
				content : content,
				closable : true
			});
		}
	}
	$(function() {
		$('#tree').tree({
			onClick : function(node) {
				if (node.text == "通用管理" || node.text == "定制化业务管理" || node.text == "基础信息管理")
					return;
				else {
					addTab(node.text, 'jsp/' + node.id + '.jsp');
				}
			}
		});
	});
</script>
<style type="text/css">
.logo {
	font-size: 18px;
	color: #00B2E3;
	line-height: 60px;
	float: left;
	padding-left: 20px;
	width: 30%;
	font-size: 20px;
	height: 56px;
}

.nav {
	float: right;
	list-style: none;
	width: 205px;
	padding-top: 25px;
}

.nav li {
	float: left;
}

.nav li a {
	text-decoration: none;
	color: #00B2E3;
}

.nav li a:hover {
	text-decoration: underline;
}

.w48 {
	width: 48px;
}

.line {
	float: left;
	padding: 0px 5px
}

.sb {
	background-color: #E9F0FA;
	height: 20px;
	border: 1px solid #A2BAD6;
	color: #00B2E3;
}

.sbname {
	color: #00B2E3;
	width: 65px;
	font-weight: bold;
	font-size: 17px;
	font-family: 'Lucida Console';
	line-height: 20px;
	float: left;
}

.tm {
	font-size: 8px;
	height: 10px;
	line-height: 8px;
	float: left;
}
</style>
</head>
<body class="easyui-layout" style="width: 100%; margin: 0px auto;">
	<div data-options="region:'north'" style="height: 80px;">
		<div style="height: 100%; background-color: #D5E0FE">
			<div class="logo">KAFKA数据发送工具</div>
		</div>
	</div>
	<div data-options="region:'east',title:'日历'" style="width: 255px;">
		<div class="easyui-calendar" style="width: 250px; height: 250px;"></div>
		<h6 style="margin-left: 10px;">常用工具链接</h6>
		<ul>
			<li><a href="http://www.json.cn/" target="_blank">json格式化工具</a></li>
			
			<li style="margin-top: 5px;"><a href="http://tool.chinaz.com/Tools/unixtime.aspx" target="_blank">时间戳</a></li>
		</ul>
	</div>
	<div data-options="region:'south',title:'关于顺丰'" style="height: 150px;">
		<h1 style="text-align: center; vertical-align: middle;">顺丰科技 版权所有
			Copyright © 2013 sf-express.com Inc. All rights reserved.</h1>
	</div>
	<div data-options="region:'west',title:'功能菜单'" style="width: 200px;">
		<ul id="tree" class="easyui-tree">
			<li><span id="rt0">基础信息管理</span>
				<ul>
					<li id="kafka_config">kafka配置</li>
				</ul></li>
			<li><span id="rt1">通用管理</span>
				<ul>
					<li id="common">数据发送</li>
				</ul></li>

			<li><span id="rt2">定制化业务管理</span>
				<ul>
					<li id="wanted">通缉</li>
					<li id="oms_delivery_change">OMS派件变更</li>
				</ul></li>
		</ul>
	</div>
	<div id="tabs" class="easyui-tabs" data-options="region:'center'">
		<div id="tt" title="我的首页"
			data-options="href:'<%=basePath%>/welcome.jsp'"></div>
	</div>

</body>
</html>