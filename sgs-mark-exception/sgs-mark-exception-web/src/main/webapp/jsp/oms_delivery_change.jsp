<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<head>
<base href="<%=basePath%>">

<title>KAFKA数据发送工具</title>
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/gray/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.5.2/demo/demo.css">
<script type="text/javascript" src="jquery-easyui-1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/jsonformat.js"></script>
<style type="text/css">
body {
	margin: 0px;
	padding: 0px;
}

#fm {
	margin: 0;
	padding: 10px 20px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}
</style>
<script type="text/javascript">
	$(function() {
		$('#dg').datagrid({
			url : 'me/kafkaData/query',
			loadMsg : '加载中，请稍后···',
			pagination : 'true',
			rownumbers : 'true',
			fit : 'true',
			fitColumns : 'true',
			singleSelect : 'true',
			striped : 'true',
			toolbar : '#opr-button',
			queryParams : {
				'topicName' : $("#topic-name").val()
			}
		});

		var pager = $('#dg').datagrid('getPager');
		$(pager)
				.pagination(
						{
							displayMsg : '总共有<span style="color:red;">{total}</span>条记录，当前显示第<span style="color:red;">{from}</span>条到<span style="color:red;">{to}</span>条记录',
							beforePageText : '第',
							afterPageText : '页 共 {pages} 页',
						});

		$.extend($.fn.validatebox.defaults.rules, {
			waybillNo : {
				validator : function(value) {
					var reg = /^[0-9]{12}$/;
					return reg.test(value);
				},
				message : '请输入12位数字'
			}
		});

		$.extend($.fn.validatebox.defaults.rules, {
			mobile : {
				validator : function(value) {
					var reg = /^1[0-9]{10}$/;
					return reg.test(value);
				},
				message : '请输入正确手机号'
			}
		});

		$("#delivery-change-type").combobox({
			onChange : function(record) {
				var curValue=$("#delivery-change-type").combobox('getValue');
				if(curValue == 1){
					$("#delivery-change-table").find("tr").eq(5).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(2).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(3).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(4).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(6).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(7).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(8).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(9).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(10).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(11).attr("style","display:block");
					$("#delivery-date").datebox({required:false});
					$("#delivery-start-time").combobox({required:false});
					$("#delivery-end-time").combobox({required:false});
					$("#dept-name").textbox({required:true});
					$("#dept-code").textbox({required:true});
					$("#dept-address").textbox({required:true});
					$("#username").textbox({required:false});
					$("#mobile").textbox({required:false});
					$("#workday-preference").combobox({required:false});
					$("#holiday-preference").combobox({required:false});
				}else if(curValue == 2){
					$("#delivery-change-table").find("tr").eq(5).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(2).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(3).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(4).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(6).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(7).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(8).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(9).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(10).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(11).attr("style","display:none");
					$("#delivery-date").datebox({required:false});
					$("#delivery-start-time").combobox({required:false});
					$("#delivery-end-time").combobox({required:false});
					$("#dept-name").textbox({required:false});
					$("#dept-code").textbox({required:false});
					$("#dept-address").textbox({required:false});
					$("#username").textbox({required:true});
					$("#mobile").textbox({required:true});
					$("#workday-preference").combobox({required:false});
					$("#holiday-preference").combobox({required:false});
				}else if(curValue == 3){
					$("#delivery-change-table").find("tr").eq(2).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(3).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(4).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(5).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(6).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(7).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(8).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(9).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(10).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(11).attr("style","display:none");
					$("#delivery-date").datebox({required:true});
					$("#delivery-start-time").combobox({required:true});
					$("#delivery-end-time").combobox({required:true});
					$("#dept-name").textbox({required:false});
					$("#username").textbox({required:false});
					$("#mobile").textbox({required:false});
					$("#workday-preference").combobox({required:false});
					$("#holiday-preference").combobox({required:false});
					$("#dept-code").textbox({required:false});
					$("#dept-address").textbox({required:false});
				}else if(curValue == 7){
					$("#delivery-change-table").find("tr").eq(2).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(3).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(4).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(5).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(6).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(7).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(8).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(9).attr("style","display:block");
					$("#delivery-change-table").find("tr").eq(10).attr("style","display:none");
					$("#delivery-change-table").find("tr").eq(11).attr("style","display:none");
					$("#delivery-date").datebox({required:false});
					$("#delivery-start-time").combobox({required:false});
					$("#delivery-end-time").combobox({required:false});
					$("#dept-name").textbox({required:false});
					$("#username").textbox({required:false});
					$("#mobile").textbox({required:false});
					$("#workday-preference").combobox({required:true});
					$("#holiday-preference").combobox({required:true});
					$("#dept-code").textbox({required:false});
					$("#dept-address").textbox({required:false});
				}
			}
		});
		
		$("#workday-preference").change(function(){
			alert($(this).val());
		})
	});
</script>
<script type="text/javascript">
	var url;
	function add() {
		$('#dlg').dialog('open').dialog('setTitle', '派件变更数据发送');
		$('#fm').form('clear');
		url = 'me/customer/sendDeliveryChangeData';
	}

	function save() {
		$('#fm').form('submit', {
			url : url,
			onSubmit : function() {
				return $(this).form('validate');
			},
			success : function(result) {
				var data = jQuery.parseJSON(result);
				if (data.success) {
					$.messager.alert({
						title : '消息',
						msg : '数据发送成功！',
						icon : 'info'
					});
					$('#dg').datagrid('reload');
					$('#fm').form('clear');
					doCancel();
				} else {
					$.messager.show({
						title : 'Error',
						msg : data.errorMessage
					});
				}
			}
		});
	}

	function refresh() {
		$('#dg').datagrid('reload');
	}

	function doQuery() {
		/* var queryParams = $('#dg').datagrid('options').queryParams;  
		alert($("#topic-query").combobox('getValue'));
		queryParams.topicName = $("#topic-query").combobox('getValue');  
		$('#dg').datagrid('options').queryParams=queryParams;        
		$("#dg").datagrid('reload');  */
		$('#dg').datagrid('load', {
			"topicName" : 'SHIVA_OMS_SGS_DELIVERY_ABNORMAL',
			"waybillNo" : $("#waybillNo-query").textbox('getValue')
		});
	}
	
	function doCancel(){
		$('#dlg').dialog('close');
		$("#delivery-change-table").find("tr").eq(2).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(3).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(4).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(5).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(6).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(7).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(8).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(9).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(10).attr("style","display:none");
		$("#delivery-change-table").find("tr").eq(11).attr("style","display:none");
	}
</script>
</head>
<body>
	<div id="opr-button" class="toolbar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td style="width: 10px">&nbsp;</td>
				<td style="font-size: 12px;">运单号：</td>
				<td><input id="waybillNo-query" class="easyui-textbox" /></td>
				<td style="width: 10px">&nbsp;</td>
				<td><a class="easyui-linkbutton" iconCls="icon-search"
					plain="true" onclick="doQuery()">查询</a></td>
				<td><a class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="add()">发送</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="refresh()">刷新</a></td>
			</tr>
		</table>
	</div>
	<table id="dg">
		<thead>
			<tr>
				<th field="waybillNo" width="80">运单号</th>
				<th field="topicName" width="80">主题名称</th>
				<th field="message" width="80">数据内容</th>
				<th field="createTime" width="120">发送时间</th>

			</tr>
		</thead>
	</table>
	<div id="dlg" class="easyui-dialog"
		data-options="closed:'true',buttons:'#dlg-button',modal:true"
		style="padding: 10px 10px;">
		<!--<div class="ftitle">派件变更数据发送</div>-->
		<form id="fm" method="post" novalidate="novalidate">
			<table id="delivery-change-table">
				<tr style="display: block">
					<td width="100px"><label style="font-size: 12px;">运单号：</label></td>
					<td><input id="waybill-no" name="waybillNo"
						class="easyui-textbox"
						data-options="missingMessage:'请输入运单号',required:'true',validType:'waybillNo'"
						style="width: 200px"></td>
				</tr>
				<tr style="display: block;">
					<td width="100px"><label style="font-size: 12px;">客户声音类型：</label></td>
					<td>
						<!-- <input id="delivery-change-type" name="deliveryChangeType"
						class="easyui-combobox" style="width: 200px"
						data-options="valueField: 'id',textField: 'text',url:'json/oms-delivery-change-type.json',required:true,missingMessage:'请选择客户声音类型',editable:false" />-->
						<select id="delivery-change-type" name="deliveryChangeType"
						class="easyui-combobox" style="width: 200px"
						data-options="required:true,missingMessage:'请选择客户声音类型',editable:false">
							<option value="1">网点自取</option>
							<option value="2">更改收件人信息</option>
							<option value="3">更改派件时间</option>
							<option value="7">用户偏好设置</option>
					</select>
					</td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">派件日期：</label></td>
					<td><input id="delivery-date" name="deliveryDate"
						class="easyui-datebox"
						data-options="missingMessage:'请选择派件日期',required:'true',editable:false"
						style="width: 200px"></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">派件开始时间：</label></td>
					<td><input id="delivery-start-time" name="deliveryStartTime"
						class="easyui-combobox" style="width: 200px"
						data-options="valueField: 'time',textField: 'time',url:'json/deliverytime.json',required:true,missingMessage:'请选择派件开始时间',editable:false" /></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">派件结束时间：</label></td>
					<td><input id="delivery-end-time" name="deliveryEndTime"
						class="easyui-combobox" style="width: 200px"
						data-options="valueField: 'time',textField: 'time',url:'json/deliverytime.json',required:true,missingMessage:'请选择派件结束时间',editable:false" /></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">网点代码：</label></td>
					<td><input id="dept-code" name="deptCode"
						class="easyui-textbox"
						data-options="missingMessage:'请输入网点名称',required:'true'"
						style="width: 200px"></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">姓名：</label></td>
					<td><input id="username" name="userName"
						class="easyui-textbox"
						data-options="missingMessage:'请输入姓名',required:'true'"
						style="width: 200px"></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">手机号：</label></td>
					<td><input id="mobile" name="mobile" class="easyui-textbox"
						data-options="missingMessage:'请输入收件号',required:'true',validType:'mobile'"
						style="width: 200px"></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">工作日偏好：</label></td>
					<td><select id="workday-preference" name="workdayPreference"
						class="easyui-combobox" style="width: 200px"
						data-options="required:true,missingMessage:'请选择派件时间段',editable:false,multiple:true">
							<option value="不派送">不派送</option>
							<option value="08:00-12:00">08:00-12:00</option>
							<option value="12:00-18:00">12:00-18:00</option>
							<option value="18:00-20:00">12:00-18:00</option>
					</select></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">双休节假日偏好：</label></td>
					<td><select id="holiday-preference" name="holidayPreference"
						class="easyui-combobox" style="width: 200px"
						data-options="required:true,missingMessage:'请选择派件时间段',editable:false,multiple:true">
							<option value="不派送">不派送</option>
							<option value="08:00-12:00">08:00-12:00</option>
							<option value="12:00-18:00">12:00-18:00</option>
							<option value="18:00-20:00">12:00-18:00</option>
					</select></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">网点名称：</label></td>
					<td><input id="dept-name" name="deptName"
						class="easyui-textbox"
						data-options="missingMessage:'请输入网点名称',required:'true'"
						style="width: 200px"></td>
				</tr>
				<tr style="display: none">
					<td width="100px"><label style="font-size: 12px;">网点地址：</label></td>
					<td><input id="dept-address" name="deptAddress"
						class="easyui-textbox"
						data-options="missingMessage:'请输入网点地址',required:'true'"
						style="width: 200px"></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="dlg-button">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-ok'" onclick="save()">保存</a> <a
			href="javascript:void(0)" class="easyui-linkbutton"
			data-options="iconCls:'icon-cancel'"
			onclick="doCancel()">取消</a>
	</div>
	<input id="topic-name" type="hidden"
		value="SHIVA_OMS_SGS_DELIVERY_ABNORMAL">
</body>
</html>

