package com.zsp.spingcloud.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 01204808 on 2017/6/30.
 */
@RequestMapping("/config")
@RestController
@RefreshScope
public class ConfigController {

    @Value("${test}")
    private String test;

    @RequestMapping("/test")
    public String getConfig(){
        return test;
    }
}
