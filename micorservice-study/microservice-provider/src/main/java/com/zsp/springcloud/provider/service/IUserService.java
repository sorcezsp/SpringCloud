package com.zsp.springcloud.provider.service;

import com.zsp.springcloud.provider.domain.User;

/**
 * Created by 01204808 on 2017/6/28.
 */
public interface IUserService {

    User getUserById(Long id);
}
