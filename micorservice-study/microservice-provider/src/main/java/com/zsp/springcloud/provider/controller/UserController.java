package com.zsp.springcloud.provider.controller;

import com.zsp.springcloud.provider.domain.User;
import com.zsp.springcloud.provider.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 01204808 on 2017/6/28.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public User getById(@PathVariable Long id){
        ServiceInstance instance = discoveryClient.getLocalServiceInstance();
        User user = userService.getUserById(id);
        logger.info("user/{}, host:{}, service_id:{}, result:{}" + user,id,instance.getHost(),instance.getServiceId(),user);
        return user;
    }
}
