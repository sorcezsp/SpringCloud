package com.zsp.springcloud.provider.service.impl;

import com.zsp.springcloud.provider.domain.User;
import com.zsp.springcloud.provider.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 01204808 on 2017/6/28.
 */
@Service
public class UserService implements IUserService {

    private static Map<Long,User> userMap = new HashMap<>();

    static {
        userMap.put(1L,new User(1l,"zsp",30));
        userMap.put(2L,new User(2l,"cwt",28));
        userMap.put(3L,new User(3l,"alex",28));
    }


    @Override
    public User getUserById(Long id) {
        return userMap.get(id);
    }
}
