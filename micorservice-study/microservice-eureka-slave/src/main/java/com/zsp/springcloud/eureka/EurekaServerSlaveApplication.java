package com.zsp.springcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by 01204808 on 2017/6/30.
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerSlaveApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerSlaveApplication.class);
    }
}
