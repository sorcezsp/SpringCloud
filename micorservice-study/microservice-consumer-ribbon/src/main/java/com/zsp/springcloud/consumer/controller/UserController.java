package com.zsp.springcloud.consumer.controller;

import com.zsp.springcloud.consumer.domain.User;
import com.zsp.springcloud.consumer.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 01204808 on 2017/6/28.
 */
@RequestMapping("/ribbon/user")
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public User getById(@PathVariable Long id){
        User user = userService.getUserById(id);
        logger.info("获取到的用户id为 {}, User为 {}", id, user);
        return user;
    }
}
