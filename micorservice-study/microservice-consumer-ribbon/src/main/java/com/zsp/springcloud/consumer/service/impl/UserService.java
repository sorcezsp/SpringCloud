package com.zsp.springcloud.consumer.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.zsp.springcloud.consumer.domain.User;
import com.zsp.springcloud.consumer.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by 01204808 on 2017/6/28.
 */
@Service
public class UserService implements IUserService {

    @Autowired
    RestTemplate restTemplate;

    @Override
    @HystrixCommand(fallbackMethod = "fallback")
    public User getUserById(Long id) {
        return restTemplate.getForObject("http://microservice-provider/user/" + id, User.class);
    }

    public User fallback(Long id){
        return new User();
    }
}
