package com.zsp.springcloud.consumer.service;

import com.zsp.springcloud.consumer.domain.User;

/**
 * Created by 01204808 on 2017/6/28.
 */
public interface IUserService {

    User getUserById(Long id);
}
