package com.zsp.springcloud.feign.service.impl;

import com.zsp.springcloud.feign.domain.User;
import com.zsp.springcloud.feign.service.IUserServiceClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by 01204808 on 2017/6/28.
 */
@Component
public class UserServiceClientHystrix implements IUserServiceClient {
    @Override
    public User getUserById(@PathVariable("id") Long id) {
        return new User();
    }
}
