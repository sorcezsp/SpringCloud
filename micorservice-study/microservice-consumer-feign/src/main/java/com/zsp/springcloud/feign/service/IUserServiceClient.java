package com.zsp.springcloud.feign.service;

import com.zsp.springcloud.feign.domain.User;
import com.zsp.springcloud.feign.service.impl.UserServiceClientHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by 01204808 on 2017/6/28.
 */
@FeignClient(value = "microservice-provider", fallback = UserServiceClientHystrix.class)
public interface IUserServiceClient {

    @RequestMapping(value = "user/{id}", method = RequestMethod.GET)
    User getUserById(@PathVariable("id") Long id);
}
