package com.zsp.springcloud.microservice.consumer.service;

import com.zsp.springcloud.microservice.consumer.domain.User;

/**
 * Created by 01204808 on 2017/8/11.
 */
public interface IUserService {

    User getUserById(Long id);
}
